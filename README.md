# Alipay

轻量的支付宝组件(A Lightweight Alipay Component)
---

+ 包引入:
	
	```xml
	<dependency>
        <groupId>me.hao0</groupId>
        <artifactId>alipay-core</artifactId>
        <version>1.0.3</version>
    </dependency>
	```
	
+ 文档见[这里](https://github.com/ihaolin/alipay)。
        